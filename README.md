# EthereumDevelopment
https://github.com/sp41mer/ - взято отсюда

Здесь лежат материалы для моего курса по разработке в блокчейне Ethereum. 


[ENVIRONMENT.md](https://gitlab.com/vkulish/titanium/blob/master/ENVIRONMENT.md) - файл, в котором описано как быстро начать разработку в блокчейне Ethereum

[Base Solidity](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/Base%20Solidity) - здесь  находятся наиболее простые версии смарт-контрактов

[Advanced](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/Advanced) - более сложные версии смарт-контрактов

[DApp Example](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/DApp%20Example) - самый простой пример реализации DApp

[ICO Contracts](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/ICO%20Contracts) - примеры реализации контрактов ICO

[Open Source Examples](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/Open%20Source%20Exapmples) - здесь разобраны наиболее часто используемые Open Source проекты

[Tokens](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/Tokens) - примеры реализации токенов

[Web3 Simple Example](https://gitlab.com/vkulish/titanium/blob/master/Ethereum/Web3%20Simple%20Example) - простой пример для взаимодействия с Web3
