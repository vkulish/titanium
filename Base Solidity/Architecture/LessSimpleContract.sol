// Указываем версию солидити для компилятора
pragma solidity ^0.4.11;


//Объявляем контракт
contract secondContract{


    // Объявляем переменную donator, в которой будет содержаться значение типа Адрес
    address public donator;

    // Public означает, что значение этой переменной будет видно всем

    // Объявляем переменную owner, в которой будет содержаться значение типа Адрес
    address public owner;



    // Объявляем переменную value, в которой будет содержаться значение типа uint
    uint public value;


    // Объявляем переменную lastTimeForDonate, в которой будет содержаться значение типа uint
    uint public lastTimeForDonate;

    // Объявляем переменную lastTimeForValue, в которой будет содержаться значение типа uint
    uint public lastTimeForValue;

    // Объявляем переменную timeOut, в которой будет содержаться заранее определенное значение типа uint
    constant uint timeOut = 120 seconds;


    // Эта функция выполнится в момент инициализации контракта
    function lessSimpleContract() public  {

        // Присваиваем функции donator значение адреса того, кто отправил эфиры
        owner = msg.sender;

    }

    function() public payable {
    // Функция для приема эфиров
    // Отсутвие названия говорит о том, что эта функция будет вызвана в момент,
    // когда на адрес контракта переводятся деньги
    // payable - модификатор который означает, что вместе с вызовом функции могут передаваться эфиры

        // Проверяем, что достаточное количество средств переведено
        require(msg.value > 1 finney);

        // Проверяем, что выполнено условие по времени
        require(lastTimeForDonate + timeOut < now);

        // Вызываем внутреннюю функцию
        setDonator(msg.sender);

    }

    function buyValue(uint _value) public payable {
    // Функция для приема эфиров и установки значения
    // Функция принимает переменную _value в формате uint
    // payable - модификатор который означает, что вместе с вызовом функции могут передаваться эфиры

        // Проверяем, что достаточное количество средств переведено
        require(msg.value > 1 finney);

        // Проверяем, что выполнено условие по времени
        require(lastTimeForDonate + timeOut < now);

        // Вызываем внутреннюю функцию
        setValue(_value);

    }

    function setValue(uint _value) internal {
    // Функция установки нового значения
    // internal означает, что эта функция недоступна для вызова вне контракта
    // (uint _value) означает, что функция принимает значение типа uint, которое потом будет
    // доступно по имени _value

        // Присваиваем переменной value значение адреса, находящегося в переменной _value
        value = _value;
        lastTimeForValue = now;


    }

    function setDonator(address _donator) internal {
    // Функция для установки нового donator
    // internal означает, что эта функция недоступна для вызова вне контракта
    // (address _donator) означает, что функция принимает значение типа адрес, которое потом будет
    // доступно по имени _donator

        // Присваиваем переменной donator значение адреса, находящегося в переменной _donator
        donator = _donator;
        lastTimeForDonate = now;
    }
}