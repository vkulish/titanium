// Указываем версию солидити для компилятора
pragma solidity ^0.4.11;

//Объявляем контракт
contract myFirstDapp{

    // Объявляем переменную donator, в которой будет содержаться значение типа адрес
    address public donator;
    // Функция для приема эфиров
    function() payable{

        // Присваиваем переменной donator значение адреса того, кто отправил эфиры
        donator = msg.sender;
    }
}